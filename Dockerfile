FROM skyrkt/nginx-node:1.0.4

EXPOSE 8080

COPY package.json typings.json /var/www/apps/lunch.skyrocket.is/
WORKDIR /var/www/apps/lunch.skyrocket.is/
RUN npm install
RUN npm run typings install

#Configure NGINX
RUN mkdir -p /var/log/lunch.skyrocket.is/ && \
    touch /var/log/lunch.skyrocket.is/nginx.access.log && \
    mkdir -p /var/log/lunch.skyrocket.is/ && \
    touch /var/log/lunch.skyrocket.is/nginx.error.log

RUN ln -sf /dev/stdout /var/log/lunch.skyrocket.is/nginx.access.log && \
    ln -sf /dev/stderr /var/log/lunch.skyrocket.is/nginx.error.log

COPY ./docker-config/nginx.conf /etc/nginx/sites-available/default.conf
COPY ./docker-config/nginx.conf /etc/nginx/sites-enabled/default.conf
COPY ./docker-config/nginx.conf /etc/nginx/conf.d/default.conf

# Copy over files
COPY ./ /var/www/apps/lunch.skyrocket.is/

RUN npm run build:prod

CMD nginx -g "daemon off;"
